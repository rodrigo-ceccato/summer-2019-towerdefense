local Bullet = new 'database.entity' {
    speed=50,
    bullet_y_limit = 2000,
    drawable_sprite = nil,
    dir = 0,
    colisionDamage = 0.5,
  }
  
  function Bullet:init()
    local drawable_sprite = new 'graphics.enemy_sprite' {
      spec = {sprite = self.sprite},
    } 
    self.drawable_sprite = drawable_sprite
    
    self.drawable_sprite.position = self.init_pos or new(Vec) { 1, 1 }
  end

  
function Bullet:update(dt)
    local pos = self:getPosition()
    local dirVec = Vec.fromAngle(self.dir)
    self.drawable_sprite.position:translate(dirVec * self.speed * dt)
end

function Bullet:isDead()
    if self.hp <= 0 then
        self:destroy()
    
        return true
    else
        local pos = self:getPosition()
        y_pos, _ = pos:get()
    
        -- delete bullets that go away from the visible screen
        if y_pos >= self.bullet_y_limit then
            self:destroy()

            return true
        end
    end
    
    return false
end
  
return Bullet
  