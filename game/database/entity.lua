local Entity = new (Object) {
  hp = 5,
  maxHp = 5,
  power = 1,
  sprite = 'none',
  speed = 0,
  drawbale_sprite = nil,
  colisionDamage = 1,
  firerate = 0,
  shooting_timer = 0,
}

function Entity:init()
  self.hp = self.maxHp
end

function Entity:update()
end

function Entity:takeDamage(damageValue)
  self.hp = self.hp-damageValue
end

function Entity:getBoxPosition()
  return self.drawable_sprite.box + self:getPosition()
end

function Entity:getPosition()
  return self.drawable_sprite.position
end

function Entity:getColisionDamage()
  return self.colisionDamage
end

function Entity:destroy()
  self.drawable_sprite:destroy()
end

function Entity:updateGrid(grid)
  -- necessary on some entities that have grid info
end

function Entity:isDead()
  if self.hp <= 0 then
    self:destroy()

    return true
  end

  return false
end

return Entity
