local Enemy = new 'database.entity' {
  reward = 0,
  speed=50,
  drawable_sprite = nil,
  dir = 180,
  retreat_cd = 0,
  retreat_time = .3,
}

function Enemy:init()
  self.W = self.W or 500
  local drawable_sprite = new 'graphics.enemy_sprite' {
    spec = {sprite = self.sprite},
  } 
  self.drawable_sprite = drawable_sprite
  
  self.drawable_sprite.position = new(Vec) { W, math.random(200,500) }
end

function Enemy:retreat()
  self.retreat_cd = self.retreat_time
end


function Enemy:update(dt)
  if self.retreat_cd >= 0 then
    self.dir = 0
    self.retreat_cd = self.retreat_cd -dt
  else
    self.dir = 180
  end

  local pos = self:getPosition()
  local dirVec = Vec.fromAngle(self.dir)
  self.drawable_sprite.position:translate(dirVec * self.speed * dt)
end

function Enemy:isDead(Gameplay)
  if self.hp <= 0 then
    Gameplay:addReward(self.reward)
    self:destroy()

    return true
  end

  return false
end

function Enemy:reachedPos(y_limit)
  local pos = self:getPosition()
  y_pos, _ = pos:get()

  if y_pos <= y_limit then
    return true
  end
  
  return false
end

return Enemy
