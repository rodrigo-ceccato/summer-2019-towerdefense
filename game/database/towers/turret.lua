local turret = new 'database.tower' {
  name = "TURRET",
  sprite = 'tesla-turret',
  power = 10,
  cost = 10,
  description = '"10/10 would buy again", but takes a while to shoot',
  hp = 300,
  firerate = 0.3,
  shooting_timer = 0,
  bullet_type = 'light-bullet',
}

return turret

