local barricate = new 'database.tower' {
  name = "SIMPLE BARRICATE",
  sprite = 'barricate',
  power = 0,
  cost = 50,
  description = 'Protects you with its life',
  hp = 50,
  firerate = 0,
  shooting_timer = 0,
  colisionDamage = 0,
  miningRate = 0,
  mining_timer = 0,
  mining_value = 0,
}

return barricate

