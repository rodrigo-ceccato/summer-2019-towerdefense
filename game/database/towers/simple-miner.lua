local turret = new 'database.tower' {
  name = "SIMPLE MINER",
  sprite = 'simple-miner',
  power = 0,
  cost = 1,
  description = 'Bury me with my... money',
  hp = 1,
  firerate = 0,
  shooting_timer = 0,
  colisionDamage = 0,
  miningRate = 5,
  mining_timer = 0,
  mining_value = 1,
}

return turret

