local superMachineGun = new 'database.tower' {
  name = "SUPER MACHINE GUN",
  sprite = 'super-sentry',
  power = 20,
  cost = 30,
  description = "Super sentry, life fast DIE YOUNG",
  firerate = 5,
  shooting_timer = 0,
  bullet_type = 'metal-bullet',
  life_expc = 5,
}

return superMachineGun 
