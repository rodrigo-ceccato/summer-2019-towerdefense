local machineGun = new 'database.tower' {
  name = "MACHINE GUN",
  sprite = 'sentry-gun',
  power = 20,
  cost = 30,
  description = "Sentry, comin' up",
  firerate = 0.9,
  shooting_timer = 0,
  bullet_type = 'arrow',

}

return machineGun 

