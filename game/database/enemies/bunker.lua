local bunker = new 'database.enemy' {
    name = "Bunker",
    sprite = 'bunker',
    power = 10,
    colisionDamage = 100,
    hp = 30,
    speed = 5,
    reward = 1000,
  }
  
return bunker
