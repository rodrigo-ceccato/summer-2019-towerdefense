local metalBullet = new 'database.bullet' {
    hp = 1,
    sprite = 'metal-bullet',
    speed = 500,
    colisionDamage = 0.5,

}

return metalBullet
