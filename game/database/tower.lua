local Tower = new 'database.entity' {
  cost = 999999,
  description = "Description not found",
  drawable_sprite = nil,
  grid_i = -1,
  grid_j = -1,
  speed = 0,
  bullet_type = 'arrow',
  miningRate = 0,
  mining_timer = 0,
  mining_value = 0,
  life_expc = 0,
  duration = 0,
  too_old_to_live = false,

}

function Tower:init()
  local drawable_sprite = new 'graphics.enemy_sprite' {
    spec = {sprite = self.sprite},
  } 
  self.drawable_sprite = drawable_sprite
end

function Tower:isDead()
  if self.hp <= 0 then
    self:destroy()
    return true
  end

  if self.too_old_to_live then
    self:destroy()
    return true
  end

  return false
end

function Tower:destroy()
  self.drawable_sprite:destroy()
end

function Tower:updateGrid(grid)
  grid:remove(self.grid_i, self.grid_j)
end

function Tower:update(dt, Gameplay)
  -- *********** SHOOT BULLETS *********
  self.shooting_timer = self.shooting_timer + dt
  while self.shooting_timer > 1 / self.firerate do
    self.shooting_timer = self.shooting_timer - 1 / self.firerate
    
    local x, y = self:getPosition():get()
    local bullet_pos = new(Vec) {x, y}
    local bullet_path = "database.bullets." .. self.bullet_type
    local new_bullet  = new (bullet_path) {
      init_pos = bullet_pos
    }
    Gameplay:createBullet(new_bullet)
  end

  -- ************* MINING ****************
  self.mining_timer = self.mining_timer + dt
  while self.mining_timer > 1 / self.miningRate do
    self.mining_timer = self.mining_timer - 1 / self.miningRate
    
    Gameplay:addReward(self.mining_value)
  end

  -- *********** AGING **************
  self.duration = self.duration + dt
  if (self.life_expc > 0) and (self.duration >= self.life_expc) then
    self.too_old_to_live = true
  end
end

return Tower
