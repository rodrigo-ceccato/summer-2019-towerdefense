
local GameOverScreen = new 'graphics.drawable' {
    filename = "game_over",
    hotspot = nil
  }
  
  function GameOverScreen:loadImage()
    self.image = love.graphics.newImage("assets/" .. self.filename .. ".png")
  end
  
  function GameOverScreen:init()
    self.hotspot = self.hotspot or new(Vec) { -.4, 0 }
    self:loadImage()
  end
  
  function GameOverScreen:getHotspot()
    local size = new(Vec) { self.image:getDimensions() }
    return self.hotspot * size
  end
  
  function GameOverScreen:onDraw()
    local g = love.graphics
    g.setColor(1, 1, 1)
    g.draw(self.image, 0, 0, 0, 1, 1, self:getHotspot():get())
  end
  
return GameOverScreen
  