
local TowerSprite = new 'graphics.sprite' {
  spec = nil
}

function TowerSprite:init()
  assert(self.spec)
  self.filename = self.spec.sprite
  self:loadImage()
  local w,h = self.image:getDimensions()
  local hotspot = self:getHotspot()
  self.box = new(Box) { -hotspot.x, w-27, -hotspot.y, h-27 }
end

return TowerSprite
