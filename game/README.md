# SUMMER 2019 TOWERDEFENSE

Projeto do curso de verão de desenvolvimento de jogos na * incrível * engine Love2D.

## Mecânicas adicionadas 

>### Inimigos
>* Criar um tipo "Enemy" que representa os inimigos tentando invadir o jogador
>* Inimigos aparecem na margem direita da tela
>* Inimigos se movem pra esquerda em linha reta

>### Derrota
>1. Se algum inimigo chegar na extremidade esquerda, você perde
>2. Mostrar uma tela de derrota

>### Vida das torres
>1. Torres possuem vida atual e máxima
>2. Toda torre começa com sua vida atual igual a sua máxima
>3. A vida máxima de cada torre deve ser especificada no banco de dados

>### Destruição de torres
>1. Quando um inimigo encosta em uma torre, ele reduz uma quantia fixa da vida
atual da torre
>2. Ao causar dano, inimigos recuam alguns passos
>3. Torres são destruídas quando ficam com zero de vida atual

>### Disparos
>1. Torres podem disparar projéteis
>2. Cada torre tem uma taxa de disparo (*firerate*) com a qual dispara projéteis
>3. Projéteis são só pequenos objetos que se movem para direita à velocidade
constante (e são destruídos se saem da tela)
>4. A taxa de disparo de cada torre deve ser determinada no banco de dados

>### Destruição de inimigos
>1. Inimigos atingidos por projéteis são destruídos, a princípio

>### Vida dos inimigos
>1. Inimigos possuem vida atual e máxima
>2. Todo inimigo começa com sua vida atual igual a >sua máxima
>3. A vida máxima de cada inimigo deve ser especificada no banco de dados
>4. Se um inimigo ficar com zero ou menos de vida atual, ele é excluído do jogo

>### Entidades
>1. Refatorar o código repetido de vida de torres e inimigos para um só lugar
>2. Fazer que ambos sejam subtipos de entidades (*entities*)


>### Poder dos inimigos
>1. Inimigos possuem um valor de poder (*power*)
>2. Esse valor é determinado pelo banco de dados
>3. Quando inimigos encostam em uma torre, eles reduzem uma quantidade de vida igual ao seu poder (ao invés de uma quantidade fixa universal)

>### Poder das torres
>1. Torres possuem um valor de poder (*power*)
>2. Esse valor é determinado pelo banco de dados
>3. Quando projéteis disparados por uma torre acertam um inimigo, eles reduzem uma quantidade de vida igual ao seu poder (ao invés de destruí-los invariavelmente)
>* Observação: na verdade o dano que os projéteis causam está determinado no banco de dados, e não depende da torre que os dispara. Além disso, cada torre tem seu tipo de projétil, e um dano que dá caso um inimigo colida com ela. Para algumas torres este dano é ZERO (isto é, ela não causa dano na colisão)

>### Progressão
>1. A quantidade de inimigos criados aumenta ao longo do tempo

>### Velocidade de inimigos
>1. Todo inimigo possui uma velocidade (*speed*)
>2. Essa velocidade determina a velocidade de movimento dos inimigos
>3. A velocidade de cada inimigo é determinada no banco de dados


>### Dinheiro
>1. Você gasta dinheiro quando constrói uma torre
>2. Você não pode construir uma torre se não tiver dinheiro para tal
>3. Você ganha dinheiro quando derrota inimigos

>### Custo de torres
>1. Toda torre possui um custo
>2. O custo de uma torre é o valor gasto para construí-la

>### Valor dos inimigos
>1. Todo inimigo possui um valor
>2. A quantidade de dinheito ganha por derrotar um inimigo é determinada pelo seu valor
> * Observação: o valor da recompensa está no atribudo reward dos inimigos

>### Mineradoras
>1. Mineradoras são torres que produzem dinheiro de tempos em tempos
>2. Toda mineradora tem uma taxa de produção com a qual ela produz dinheiro
>3. A taxa de produção de cada mineradora é determinada no banco de dados

>### Torres temporárias
>1. Torres podem ter uma duração, que indica quantos segundos elas ficam em jogo antes de serem destruídas por desgaste
>2. Para isso, elas também precisam de um contador de tempo, que ajuda a detectar quando a duração da torre passou
>3. A duração de cada torre deve ser determinada no banco de dados
>4. Uma duração de zero indica que a torre dura pra sempre ou até ser destruída
> * Observação: apenas a SUPER MACHINE GUN tem tempo de vida, que é de 5 seugndos

### Mecânicas Extras
> ### Há dois tipos de inimigos

> ### Há três tipos de bullets

> ### Há uma torre extra

> ### Há uma torre barricada

> ### HÁ UMA TORRE fofa CHAMADA CASSIOTORRE, favor experimentar :)
