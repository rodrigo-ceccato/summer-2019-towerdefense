local Gameplay = new 'state.base' {
  graphics = nil,
  DCopy = nil,
  grid = nil,
  counter = nil,
  tower_specs = nil,
  buy_buttons = nil,
  enemy_specs = nil,
  selected = 1,
  page = 1,
  page_num = 1,
  enemy_list = {},
  tower_list = {},
  bullet_list = {},
}

function Gameplay:onEnter(graphics)
  self.graphics = graphics
  self:loadTowerSpecs()
  self:loadEnemySpecs()
  self:createGrid()
  self:createBuyButtons()
  self:createArrowButtons()
  self:createCounter()
  self:changeTowers(1)

  -- set enemy spawn timer variavles
  self.enemyCd = 0
  self.fixedCd = 5
end

function Gameplay:loadEnemySpecs()
  self.enemy_specs = {}
  for i,specname in require 'database' :each('enemies') do
    self.enemy_specs[i] = require('database.enemies.' .. specname)
    self.enemy_specs[i]._filename = specname
  end
end

function Gameplay:loadTowerSpecs()
  self.tower_specs = {}
  for i,specname in require 'database' :each('towers') do
    self.tower_specs[i] = require('database.towers.' .. specname)
    self.tower_specs[i]._filename = specname
  end
  self.page_num = math.ceil(#self.tower_specs / 4)
end

function Gameplay:changeTowers()
  for i=1,4 do
    local k = (self.page - 1) * 4 + i
    self.buy_buttons[i].tower_spec = self.tower_specs[k]
  end
  self:selectTower(1)
end

function Gameplay:createGrid()
  self.grid = new 'graphics.grid' {}
  self.grid.selected_callback = function(i, j) self:buildTower(i, j) end
  self.graphics:add('bg', self.grid)
end

function Gameplay:createBuyButtons()
  self.buy_buttons = {}
  local W,H = love.graphics.getDimensions()
  for i=1,4 do
    local buy_button
    buy_button = new 'graphics.buy_button' { index = i }
    buy_button.callback = function() self:selectTower(i) end
    self.buy_buttons[i] = buy_button
    self.graphics:add('gui', buy_button)
  end
end

function Gameplay:createArrowButtons()
  local W,H = love.graphics.getDimensions()
  self.graphics:add('gui', new 'graphics.arrow_button' {
    side = 'left',
    position = new(Vec) { 240 / 4, 3 * H / 4 + 40 },
    callback = function()
      self.page = math.max(1, self.page - 1)
      self:changeTowers()
    end
  })
  self.graphics:add('gui', new 'graphics.arrow_button' {
    side = 'right',
    position = new(Vec) { 3 * 240 / 4, 3 * H / 4 + 40 },
    callback = function()
      self.page = math.min(self.page_num, self.page + 1)
      self:changeTowers()
    end
  })
end

function Gameplay:createCounter()
  self.counter = new 'graphics.counter' {}
  self.counter:add(100)
  self.graphics:add('gui', self.counter)
end

function Gameplay:buildTower(i, j, spec)
  local chosenTower = self.buy_buttons[self.selected].tower_spec
  local tower_path = "database.towers." .. chosenTower._filename
  
  local counter = self.counter
  local towerCost = chosenTower.cost

  if counter:getValue() >= towerCost then
    counter:subtract(towerCost)

    local new_tower = new (tower_path) {grid_i = i, grid_j = j}
    self.graphics:add('entities', new_tower.drawable_sprite)
    self.grid:put(i, j, new_tower.drawable_sprite)
    table.insert(self.tower_list, new_tower)
  end
  
end

function Gameplay:createBullet(bullet)
  self.graphics:add('entities', bullet.drawable_sprite)
  table.insert(self.bullet_list, bullet)
end

function Gameplay:selectTower(i)
  local button = self.buy_buttons[i]
  if button.tower_spec then
    self.buy_buttons[self.selected].selected = false
    button.selected = true
    self.selected = i
  end
end

function Gameplay:spawnEnemy(enemyName)
  W = self.grid:getRightSideY()

  local enemy_path = "database.enemies." ..enemyName

  local new_enemy  = new (enemy_path) {W = W}
 
  self.graphics:add('entities', new_enemy.drawable_sprite)
  table.insert(self.enemy_list, new_enemy)
end

function Gameplay:generateEnemies(dt)
  self.enemyCd =  self.enemyCd - dt
  if self.enemyCd <= 0 then
    self.enemyCd = self.fixedCd

    -- makes the game enemy spawn go faster each iteration
    self.fixedCd = self.fixedCd * 0.99

    -- selects a random enemy to spawn
    local i = math.random(1,#self.enemy_specs)
    local randomEnemyName = self.enemy_specs[i]._filename
    self:spawnEnemy(randomEnemyName)
  end
end

function Gameplay:addReward(value)
  self.counter:add(value)
end

function Gameplay:updateEnemies(dt)
  for _, enemy in pairs(self.enemy_list) do
    -- ********CHECK IF ENEMIES REACHED LEFT SIDE ****
    if enemy:reachedPos(350) then
      self.stack:push('defeated', self.graphics)
    end

    -- ******** CHECK IF ENEMIES ARE HITING TOWERS ****
    for _, tower in pairs(self.tower_list) do
      if (enemy:getBoxPosition()):intersects(tower:getBoxPosition()) then
        enemy:retreat()

        local dmg = enemy:getColisionDamage()
        self:damageNotification(dmg, tower:getPosition())
        tower:takeDamage(dmg)

        local dmg = tower:getColisionDamage()
        self:damageNotification(dmg, enemy:getPosition())
        enemy:takeDamage(dmg)
      end
    end

    -- ********** CHECK IF BULLETS ARE HITTING ENEMIES *****
    for _, bullet in pairs(self.bullet_list) do
      if (bullet:getBoxPosition()):intersects(enemy:getBoxPosition()) then
        local dmg = bullet:getColisionDamage()
        self:damageNotification(dmg, enemy:getPosition())
        enemy:takeDamage(dmg)
        bullet:takeDamage(dmg)
      end
    end
  end
  
  -- ******* UPDATE AND CHECK IF SOME ENTITY IS DEAD ******
  local lists = {self.enemy_list, self.tower_list, self.bullet_list}
  for _, list in pairs(lists) do
    local removed = {}
    for key, ent in pairs(list) do
      ent:update(dt, self)
      if ent:isDead(self) then
        ent:updateGrid(self.grid)
        removed[key] = true
      end
    end

    -- ***** REMOVES DEAD ENTITIES *****
    for i=#list,1,-1 do
      if removed[i] then
        table.remove(list, i)
      end
    end
  end
end

function Gameplay:damageNotification(value, position)
  local not_pos = new(Vec) {position.x,
                            position.y}
  local notification = new 'graphics.notification' {
    position = not_pos,
    text = "-" .. value
  }
  self.graphics:add('fx', notification)
end

function Gameplay:onUpdate(dt)
  self:generateEnemies(dt)
  self:updateEnemies(dt)
end

return Gameplay
