local Defeated = new 'state.base' {

}

function Defeated:onEnter(graphics)
    self.graphics = graphics

    self.graphics:add('gui', new 'graphics.game_over_screen' {})

end

return Defeated
